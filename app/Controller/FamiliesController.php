<?php
App::uses('AppController', 'Controller');
/**
 * Families Controller
 *
 *
 */
class FamiliesController extends AppController {

	public function index()
	{
		$viewFamilies=$this->getViewArrayForFamilies($this->getFamilies());

		$this->set('families',$viewFamilies);
	}

	private function getFamilies(){
		$this->Family->recursive = -1;

		$contain = array(
			'Furniture' => array(
				'Image'=>array(
					'name',
					'alt',					
					'conditions'=>array('Image.name LIKE' => '%main.jpg%')
					),
				'FurnitureType' => array(
						'hash'
					)
				),
			'Image'=>array(
				'name','alt'
				)
			);

		$families = $this->Family->find('all',
			array(
				'contain'=>$contain,
				'fields'=>array('name','hash')
				)
			);
		return $families;
	}

	private function getViewArrayForFamilies($families){
		$viewFamilies = array();
		foreach ($families as $key => $family) {
			$furnitures = array();
			foreach ($family['Furniture'] as $key => $furniture) {				
				$furnitures[] = array(
					'sheetUrl'=> '"'.Router::url('/',true).'butor/adatlap/'.$furniture['FurnitureType']['hash'].'/'.$family['Family']['hash'].'/'.$furniture['hash'].'"',
					'name'=>$furniture['name'],
					'hash'=>$furniture['hash'],
					'type'=>$furniture['FurnitureType']['hash'],
					'image'=>array(
						'src'=>$this->getImageSrc('furnitures',$family['Family']['hash'],$furniture['Image'][0]['name']),
						'alt'=>'"'.$furniture['Image'][0]['alt'].'"'
						)
				);
			}

			$viewFamilies[]=array(
				'name'=>$family['Family']['name'],
				'hash'=>$family['Family']['hash'],
				'image'=>array(
					'src'=>$this->getImageSrc('furnitures',$family['Family']['hash'],'family',$family['Image'][0]['name']),
					'alt'=>'"'.$family['Image'][0]['alt'].'"'
					),
				'furnitures'=>$furnitures
				);
		}
		return $viewFamilies;
	}

}
