<?php
App::uses('AppController', 'Controller');
/**
 * Furnitures Controller
 *
 */



class FurnituresController extends AppController {



	public function index(){

		$furnitureTypes = $this->getFurnitureTypes();

		$viewFurnitureTypes = $this->getViewArrayForTypes($furnitureTypes);

		$this->set('furnitureTypes', $viewFurnitureTypes);


	}
	var $valami;

	public function adatlap($furnitureType=null, $family=null, $furnitureHashName=null){

		$furniture=$this->getFurniture($furnitureType, $family, $furnitureHashName);



		$viewFurniture = $this->getViewArrayForFurniture($furniture);
		//debug($viewFurniture);

		$this->set('furniture',$viewFurniture);
		
		//$mainImageUrl = '"'.Router::url('/',true).'img/images/furnitures/'.$family.'/'.$furnitureHashName.'"';
		/*
		$this->loadModel('Image');

		$Image = new Image();
		$mainImageUrl = '/images/furnitures/'.$family.'/'.$furnitureHashName.'_main.jpg';
		$mainImage = $Image->find('first',array(
			'conditions'=>array(
				'name'=> $mainImageUrl
				)
			)
		);*/

	}


	/**
	* Lekérés TÍPUS SZERINTI listázáshoz
	*
	*/
	private function getFurnitureTypes(){
		$this->loadModel('FurnitureType');
		$FurnitureType = new FurnitureType();
		$FurnitureType->recursive = -1;

		$contain = array(
			'Furniture' => array(
				'Image'=>array(
					'name',
					'alt',
					'conditions'=>array('Image.name LIKE' => '%main.jpg%')
					),
				'Family'=>array('hash')
				),
			'Image'=>array(
				'name','alt'
				)
			);

		$furnitureTypes = $FurnitureType->find('all',
			array(
				'contain'=>$contain,
				'fields'=>array('name','hash')
				)
			);
		return $furnitureTypes;
	}

	/**
	* BÚTOR ADATOK lekérdezése
	*
	*
	***/
	private function getFurniture($furnitureType, $family, $furnitureHashName){
		$this->Furniture->recursive = -1;

		$contain = array(
			'Family'=>array(
				'conditions'=>array('Family.hash' => $family)
				),
			'Image'=>array(
				'name',
				'alt',
				'conditions'=>array('Image.name LIKE' => '%main.jpg%')
				),
			'ManyImages'=>array(
				'name',
				'alt',
				'conditions'=>array('ManyImages.name NOT LIKE'=>'%main.jpg%')
				)
			);

		$furniture = $this->Furniture->find('first',array(
			'contain'=>$contain,
			'conditions'=>array(
				'Furniture.hash'=>$furnitureHashName
				)
			)
		);

		return $furniture;
	}

	private function getViewArrayForTypes($furnitureTypes){
		$viewFurnitureTypes = array();
		foreach ($furnitureTypes as $key => $furnitureType) {
			$furnitures = array();
			foreach ($furnitureType['Furniture'] as $key => $furniture) {				
				$furnitures[] = array(
					'sheetUrl'=> '"'.Router::url('/',true).'butor/adatlap/'.$furnitureType['FurnitureType']['hash'].'/'.$furniture['Family']['hash'].'/'.$furniture['hash'].'"',
					'name'=>$furniture['name'],
					'hash'=>$furniture['hash'],
					'type'=>$furnitureType['FurnitureType']['name'],
					'image'=>array(
						'src'=>$this->getImageSrc(array('furnitures',$furniture['Family']['hash'],$furniture['Image'][0]['name'])),
						'alt'=>'"'.$furniture['Image'][0]['alt'].'"'
						)
				);
			}

			$viewFurnitureTypes[]=array(
				'name'=>$furnitureType['FurnitureType']['name'],
				'hash'=>$furnitureType['FurnitureType']['hash'],
				'image'=>array(
					'src'=>$this->getImageSrc('furniture_types',$furnitureType['Image']['name']),
					'alt'=>'"'.$furnitureType['Image']['alt'].'"'
					),
				'furnitures'=>$furnitures
				);
		}
		return $viewFurnitureTypes;
	}

	private function getViewArrayForFurniture($furniture){
		$viewFurnitures = array(
			'name'=>$furniture['Furniture']['name'],
			'description'=>$furniture['Furniture']['description'],
			'mainImage'=>array(
				'src'=>$this->getImageSrc('furnitures',$furniture['Family']['hash'],$furniture['Image'][0]['name']),
				'alt'=>$furniture['Image'][0]['alt']
				),
			'images'=>array()
			);
		foreach ($furniture['ManyImages'] as $key => $image) {
			$viewFurnitures['images'][]=array(
				'src'=>$this->getImageSrc('furnitures',$furniture['Family']['hash'],$image['name']),
				'alt'=>'"'.$image['alt'].'"'
				);
		}

		return $viewFurnitures;
	}

}
