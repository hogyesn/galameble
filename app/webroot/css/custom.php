<?php
header("Content-type: text/css");
$webroot = $_GET['webroot'];
?>
html{
	font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;font-weight:500;letter-spacing:.125em;display:block;box-sizing:border-box;

}
body{
	position: relative;
	width: auto !important;
	background-color: black;
	padding: 0 !important;

}

body:after{
	display: none;
}
body *{
	width: 100%;

	}

	.gm-preloader{

		background-color:#242024;
		bottom:0;height:100%;
		left:0;
		position:fixed;
		right:0;
		top:0;
		z-index:9999999999
	}
	.gm-preloader-img{
		background-image:url("<?php echo $webroot; ?>img/gm-preloader.gif");
		background-position:center;
		background-repeat:no-repeat;
		height:64px;
		left:50%;
		margin:-32px 0 0 -32px;
		position:absolute;
		top:50%;
		width:64px
	}
main{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	opacity: 1;
	filter: alpha(opacity=100);
	background-color: white;
}
main.opacity{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	opacity: 0.5;
	filter: alpha(opacity=50);
}

a {
}


h2{
	position: relative;
	z-index: 1;
	padding: 20px 0 0 0;
	color: white;
	font-size: 28px;
}
h3
{
	color: black;	
}
	#contact-accordion h3{
		border-bottom: 1px solid #C0C0C0;
	}
h4{
	color: black;
	font-size: 16px;
	padding-bottom: 5px;
	border-bottom: 1px solid #C0C0C0;
	margin-bottom: 5px;
	width: 90%;
}
ul{
	list-style: outside disc;
	font-size: 12px;
}
li{
	margin-top: 6px; 
}
strong{
	padding-top: 24px;
	margin-bottom: 6px;
}
p{
	margin-top: 6px;
	font-size: 12px;
	position: relative;
	z-index: 1;
}
	p.bottom{
		position: absolute;
		bottom: 2%;
		left: 10%;
	}
img{
	display: block;
}
a:hover{
	color: white;
}
.column{
	display: block;
	margin: 0 !important;
	padding:0 !important;
	overflow: hidden;
}
.abs{
	z-index: 0;
	position: absolute;
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	top: 0;
	left: 0;
}
.fixable.fixed{
	position: fixed;
	width: 25% !important;
	top: 0;
	-webkit-transition-duration: top 0.5s;
	        transition-duration: top 0.5s;
	
	
}
.fixable.topped{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	top: 90px;
}
.medium-12,.small-12{
	margin:0;
}
.padding-20{
		padding-left: 20px !important;
	padding-right: 20px !important;
	padding-bottom: 20px !important;
	padding-top: 20px !important;
}
.tile{
	height: 0 !important;
	padding-left: 20px !important;
	padding-right: 20px !important;
	padding-bottom: 25% !important;
}
	.tile.pad-720{
		height: auto !important;
		padding-bottom: 0 !important;
	}
	.tile.ht-parent{
		padding-left: 0 !important;
		padding-right: 0 !important;
	}
		.half-tile
		{
			height: 0!important;
			padding-left: 20px !important;
			padding-right: 20px !important;
			padding-bottom: 50% !important;
		}
	.tile-120
	{
		height: 95px !important;
	}
	.double-tile{
		position: relative;
		height: 0 !important;
		padding-bottom: 37.5% !important;
	}
.opacitive{
	background-color: black;
}
	
	.opacitive img{
		display: inline-block;
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		opacity: 0.65;
		filter: alpha(opacity=65);
	}
		.opacitive:hover img{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			opacity: 1;
			filter: alpha(opacity=100);
		}
		.opacitive:hover span.icon{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			opacity: 0.2;
			filter: alpha(opacity=20);
		}
img.opacitive {
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	opacity: 0.65;
	filter: alpha(opacity=65);
}
	img.opacitive:hover{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		opacity: 1;
		filter: alpha(opacity=100);
	}
div.title{
	padding-left: 20px;
	padding-right: 20px;
	padding-top: 20px;
	border-top: 85px solid white;
}
	div.title.lower{
		border-top: 155px solid white;
	}

	
/* Main HEADER */
.double
{
	width: 200%;

}
.double .column
{
	float: left !important;
}
.double img{
	height: 80%;
}
.furn-menu
{
	overflow-x: hidden;
	max-height: 0 !important;
	-webkit-transition: max-height 0.5s ease-out;
	        transition: max-height 0.5s ease-out;
	


	}
	.furn-menu.opened
	{

		max-height: 1000px !important; 
		-webkit-transition: max-height 0.5s; 
		        transition: max-height 0.5s;

	}
	.furn-nav {
		height: 50px;background-color: white !important;
	}	
		.furn-nav .active{
			border-top: 10px solid #89CCCA !important;
			
		}
		.furn-nav .trig{
			height: 50px;

			vertical-align: middle;
			border-top: 10px solid white;
		}
	.furn-menu a {
		display: inline-block;
		color: gray;
		text-align: center;
		vertical-align: middle;
		
		line-height: 50px;
		font-size: 10px;

	}
		.furn-menu a:hover{
			color: black;
		}
		.furn-menu .active a
		{
			color: black;
		}
	.furn-bar
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		position: relative;
		left: 0px;
	}
	.furn-bar.inactive{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		left: -50%;
	}
		.search{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			background-color: #242024;
			height: 50px !important;	
		}
		.search:hover{
			background-color: #6C7377;
		}
		.x .x{
			-webkit-transition-duration:0.5s;
			        transition-duration:0.5s;
			-webkit-transform: rotate(0deg);
			    -ms-transform: rotate(0deg);
			        transform: rotate(0deg);
		}
		.x:hover .x{
			-webkit-transition-duration:0.5s;
			        transition-duration:0.5s;
			-webkit-transform: rotate(90deg);
			    -ms-transform: rotate(90deg);
			        transform: rotate(90deg);
		}
			.search a,.x a{
				height: 50px !important;		
			}
				.search .src, .x .x{
					float: none;
					display: inline-block;
					width: 50px;
					height: 50px;
				}



header{
	position: fixed;
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	z-index: 999;
	top: 10px;
	height: auto !important;
}
header.scrolled
{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	top: 0;

}
	header.scrolled nav ul li a.top-bord
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		background-color: #242024;
	}
	header.hidden
	{
		-webkit-transition-duration:0.5s;
		        transition-duration:0.5s;
		top: -90px;

	}

nav {
	width: 100%;
	height: 75px;
	float: right;
	
}

	nav .nav .large-2, nav .nav .medium-2, nav .nav .small-2
	{
		width: 19.9999% !important;
	}
	nav .nav .large-1, nav .nav .medium-1, nav .nav .small-1{
		width: 9.9999% !important;
	}
	nav ul {
		list-style: none;	
		margin: 0;
		padding: 0 ;
		
		-webkit-transition-duration: 0.5s;
		
		        transition-duration: 0.5s;
		background-color: none;
	}

		nav ul > li {
				display: inline-block;
			    zoom:1;
			    *display:inline;				
			    padding: 0;
				margin: 0


		}
		nav ul  li a{
			height: 100%;
			
			display: block;
			padding: 17px 28px !important;
			margin-right: auto;
			margin-left: auto;
			
		}

		nav ul  li:hover a.top-bord{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			border-top: 10px solid rgba(137, 204, 202, 1)!important;
		}
		nav ul li a.top-bord{
			height: 86px;
			
			background-clip: padding-box !important;
			padding: 22px 0px 0px !important;
			text-align: center;
			width: 100%;
			vertical-align: middle;
		}
		.top-bord{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			border-top: 10px solid rgba(137, 204, 202, 0.3) !important;
			


		}
		/* IKONOK */
			.logo{
				float: left;
				position: relative;
				height: 76px;
				display: inline-block;
				background-color: #242024;
				top: 10px;
				text-align: center;
				vertical-align: middle;

			}
				.logo a{
					background: url("<?php echo $webroot; ?>img/icons.png");
					position: relative;
					display: inline-block;
					height: 75px !important;
					background-clip: border-box;
					min-width: 100px !important;
					width: 127px;
					
					background-origin: padding-box;
					background-position: 0px 0px;
					background-repeat: no-repeat;
					background-size: 750px 300px;
				}

			a.top-bord .nav{
				
				display: inline-block;

				height: 34px;
				width: auto;
				margin-left: auto !important; margin-right: auto !important;
				text-align: center;
				vertical-align: middle;
			}
				a.top-bord .nav .text{
					float: left;
					display: inline-block;
					width: auto;
					
					color: #fff;
					font-size: 17px;
					line-height: 35px;
				}

				.icon{
					float: left;
					background: url("<?php echo $webroot; ?>img/icons.png");		

					background-clip: border-box;
					background-origin: padding-box;
						
						background-repeat: no-repeat;					
						background-size: 750px 300px;

					border-radius: 50%;
					border: 1px solid white;
					height: 35px;
					margin-right: 10px;
					width: 35px;
					display: inline-block;

				}
					.icon.furn{
						background-position: -621px -21px !important;
					}
					.icon.places{
						background-position: -246px -20px !important;
					}
					.icon.wkning{
						background-position: -321px -21px !important; 
					}
					.icon.contact{
						background-position: -396px -20px !important;
					}
					.icon.fb,.icon.lan,.icon.src,.icon.x, .icon.arr-down{border: none !important;
						margin: 0 !important;}
					.icon.fb{
						
						background-position: -470px -20px !important;
					}
					.icon.lan{background: none !important;}
					.icon.src{
						background-position: -164px -10px;
					}
					.icon.x{
						background-position: -688px -10px;
					}
					.icon.arr-down{
						background-position: -95px -102px;
						float: right;
					}
					.icon.pdf{
						position: absolute;
						border: none;
						border-radius: 0 !important;
						width: 49px;
						height: 66px;
						background-position: -389px -80px;
						top: 50%;
						left: 50%;
						-webkit-transform: translate(-50%, -50%);
						    -ms-transform: translate(-50%, -50%);
						        transform: translate(-50%, -50%);
					}
					.icon.creator
					{
						margin: 0 !important;
						position: absolute;
						border: none;
						border-radius: 0 !important;
						width: 60px;
						height: 66px;
						background-position: -608px -80px;
						top: 50%;
						left: 50%;
						-webkit-transform: translate(-50%, -50%);
						    -ms-transform: translate(-50%, -50%);
						        transform: translate(-50%, -50%);
					}
					.icon.func{
						border-radius: 5px;
						border: 1px solid black;
						height: 32px;
						width: 32px;
						margin-bottom: 10px;
					}
						.icon.func1{
							background-position: -23px -173px !important;
						}
						.icon.func2{
							background-position: -98px -173px !important;
						}
						.icon.func3{
							background-position: -173px -173px !important;
						}
						.icon.func4{
							background-position: -248px -173px !important;
						}
						.icon.func5{
							background-position: -323px -173px !important;
						}
						.icon.func6{
							background-position: -398px -173px !important;
						}
						.icon.func7{
							background-position: -473px -173px !important;
						}
						.icon.func8{
							background-position: -548px -173px !important;
						}
						.icon.func9{
							background-position: -623px -173px !important;
						}
						.icon.func10{
							background-position: -698px -173px !important;
						}
												.icon.func11{
							background-position: -23px -248px !important;
						}
						.icon.func12{
							background-position: -98px -248px !important;
						}
						.icon.func13{
							background-position: -173px -248px !important;
						}
						.icon.func14{
							background-position: -248px -248px !important;
						}
						.icon.func15{
							background-position: -323px -248px !important;
						}
						.icon.func16{
							background-position: -398px -248px !important;
						}
						.icon.func17{
							background-position: -473px -248px !important;
						}
						.icon.func18{
							background-position: -548px -248px !important;
						}
						.icon.func .desc{
							display: none;
						}
						.icon.func:hover{
							-webkit-transition-duration:0.5s;
							        transition-duration:0.5s;
							background-color: white;
						}


			.mobile-nav{
				display: none !important;

			}
			.stat-menu{
				position: relative;
				top: 10px;
			}
				.stat-menu ul{
					font-size: 16px;
				}
.percent20{
	width: 20%;
}

/* HEADER SLIDER */
.slider-container{
	position: relative;
	z-index: 0;
	overflow: visible;
	margin-bottom: 50px !important;

}
	.slider {
		
		margin: 0;
		position: relative;
		overflow: hidden;

	}
		span.gradient{
			position: absolute; top: 0; left: 0;
			width: 100%;height: 50%;
			background-image: -webkit-linear-gradient(rgba(0,0,0,0.8), rgba(0,0,0, 0));
			background-image: linear-gradient(rgba(0,0,0,0.8),rgba(0,0,0, 0));
		}

	.slider-nav{
		margin: 0;
		padding: 0;
		height: 10px;
		position: absolute;
		z-index: 2;
		bottom: -10px;


	}
		.bands > ul {
			height: 100%;
			list-style: none;	
			margin: 0;
			padding: 0 ;
			font-size: 0;
		}
			.bands >  ul > li{
				padding: 0;
				margin: 0;
			 	display: inline-block;
			    zoom:1;
			    *display:inline;
			   width: 20%;
			   font-size: 10px;
			   cursor: pointer;
			}
			.slick-dots li button {
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				background: #B7BCC2;
				height: 100%;
				border: none;
				cursor: pointer;
				color: transparent;
			}
			.slick-dots li button:hover{
				-webkit-transition-duration:0.5s;
				        transition-duration:0.5s;
				background: #383238;
			}
			.slick-dots li.slick-active button{
				-webkit-transition-duration:0.5s;
				        transition-duration:0.5s;
				background: #383238;
			}
		.arrows{
			
			z-index: 2;
			position: absolute;
			bottom: 0;
			right: 0;
			height: 16.666%;
			width: 11.11%;
		}
			.arrows button{
				width: 50%;
				position: relative;
				right: 0;
			}

			.cnt{
				width: 5.555%;
				height: 16.666%;
				background: #383238;
				margin: 0;
				padding: 0;

				right:11.11%;
				bottom: 0;
				position:absolute;
				z-index: 3;

			}
				.cnt span{
					position: absolute !important;
					
					width: 50%;
					left: 50%;
					top: 50%;
					font-family:GM-H, 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
					color:#89ccca;
					font-weight:500;				
					letter-spacing:0.125em;
					font-size: 48px;
					transform:translate(-50%,-50%);
					-moz-transform:translate(-50%, -50%);
					-ms-transform:translate(-50%,-50%);
					-o-transform:translate(-50%,-50%);
					-webkit-transform:translate(-50%,-50%);
				}

			.slick-prev,
			.slick-next{
				
				width: 33.32%;
				height: 100%;
				background-color: rgba(137, 204, 202, 0.5);
				border: none;
				color: transparent !important;
				outline-color: transparent;
				outline-style: none;
				outline-width: 0px;

			}

				.slick-prev{
					background-image: url("<?php echo $webroot; ?>img/gm-arrow-left.png");
					background-repeat: no-repeat;
					background-position: 50% 50%;

				}
				.slick-next{
					background-image: url("<?php echo $webroot; ?>img/gm-arrow-right.png");
					background-repeat: no-repeat;
					background-position: 50% 50%;

				}







	.show-for-720-down{
		display: none;
	}

/*furnitures*/

span.black-bg a{
	background-color: black;
	display: inline-block;
}

	a:hover .furnitures{
		-webkit-transform: scale(1.1);
		    -ms-transform: scale(1.1);
		        transform: scale(1.1);
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		opacity: 1;
		filter: alpha(opacity=100);

	}
	a:hover span.border{
		-webkit-transition-duration: 0.5s,0.5s,0.5s,0.5s;
		        transition-duration: 0.5s,0.5s,0.5s,0.5s;
		height: 80%;
		top: 10%;
		opacity: 1;
		filter: alpha(opacity=100);
		border: 1px solid white;
	}
	span.border{
		-webkit-transition-duration: 0.5s,0.5s,0.5s,0.5s;
		        transition-duration: 0.5s,0.5s,0.5s,0.5s;
		
		height: 20%;
		top:40%;
		opacity: 0.5;
		filter: alpha(opacity=50);
		border-left: 1px solid white;
		border-right: 1px solid white; 
		

		right: 10%;
		width: 80%;
		position: absolute;
		
		
	}
	span.title, span.hover-title{
		margin:  auto;
		position: absolute;
		text-align: center;
		left:50%;
		top:50%;
		-webkit-transform:translate(-50%, -50%);
		-ms-transform:translate(-50%, -50%);
		transform: translate(-50%, -50%);
		color: white;font-size: 1em;
	}
.nowadays img
{
	border-bottom: medium none;
	border-right: 1px solid #C0C0C0;
	border-top: 1px solid #C0C0C0;
	min-height: 200px;
}
	.nowadays span.title
	{
		top: auto;
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		color: black;
		bottom: 0%;
	}
	.nowadays span.hover-title
	{
		-webkit-transition-duration: 0.2s;
		        transition-duration: 0.2s;
		top: -25%;
		color: gray;
	}
		.nowadays:hover .hover-title
		{
			-webkit-transition-duration: 0.2s;
			        transition-duration: 0.2s;
			top: 15%;
		}
		.nowadays:hover .title{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			color: gray;
		}
.large-img{
	position: relative;
}
.large-img:target{
	-webkit-transition:all 1s ease;
	        transition:all 1s ease;
}
.large-img .large-title{
	text-align: left;
	vertical-align: text-bottom;
	display: inline-block;
	color: white;
	font-size: 2em;
	width: 100%;
	position: absolute;
	height: 50%;
	bottom: 0;
	left: 0;
	padding-top: 15%;
	padding-left: 20px !important;
	background: -webkit-linear-gradient(rgba(255,255,255,0), rgba(50,50,50,0.95));
	background: linear-gradient(rgba(255,255,255,0),rgba(50,50,50,0.95));

}
/* news */
.news{
	margin-bottom: 0px !important;
	padding-bottom: 0px !important;
}
.news-offer{
	position: relative;

	display: inline-block;	
	background-color: #242024;
	color: white;
}
	.news-offer p{
		color: gray;
	}
.newsslider, .collslider{
	background: white;
}
	.newsslider:hover .slick-next, .newsslider:hover .slick-prev,
	.collslider:hover .slick-next, .collslider:hover .slick-prev{
		display: inline!important;
	}
	.newsslider .slick-next, .newsslider .slick-prev,
	.collslider .slick-next, .collslider .slick-prev
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		background-color: #242024;
		position: absolute;
		display: none !important;
		bottom: 45%;
		height: 50px;
		width: 50px;
	}
		.newsslider .slick-prev,
		.collslider .slick-prev
		{
			left: 0;
		}
			.newsslider .slick-prev:hover, .newsslider .slick-next:hover,
			.collslider .slick-prev:hover, .collslider .slick-next:hover{
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				background-color: #5C575E
			}
		.newsslider .slick-next,
		.collslider .slick-next{
			right: 0;
		}

/*features*/
.inc-line h2:after
{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	background: none repeat scroll 0% 0% #FFF;
	content: "";
	background-color: white;
	display: block;
	height: 10px;
	margin: 0 0px 0px;
	opacity: 0.5;
	filter: alpha(opacity=50);
	width: 50px;
}

	.inc-line:hover h2:after
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		opacity: 1;
		filter: alpha(opacity=100);
		width: 100px;
	}
div.upward .upwarding.content h2
{
	text-align: center;
	padding-bottom: 80px;
}
	div.upward:hover .upwarding.content
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		top: -120px;
	}

span.dropping{
	position: absolute;
	display: block;
	width: 100%;
	text-align: center;
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	bottom: -20px;
}
	.dropping:hover .dropping
	{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		bottom:45%;
	}
span.upward{

	position: relative;
	display: block;
	text-align: center;
	border: 1px solid white;
	height: 50px;
	padding: 5px;
	overflow: hidden;
	z-index: 10;

}
	span.upward.bottom{
		position: absolute;
		margin-bottom: 0px;
		width: 90%;
		left: 5%;
		top: 70%;
	}
	span.upward .upwarding.text{
		position: absolute;
		left: 0;
		top: 10px;
	}
	span.upward:hover .upwarding.text{
		-webkit-animation-name: upward_hover;
		        animation-name: upward_hover;
		-webkit-animation-duration: 0.5s;
		        animation-duration: 0.5s;

	}
	@-webkit-keyframes upward_hover{
		0%{
			opacity: 1;
			filter: alpha(opacity=100);
		}
		73%{
			top: -50px;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		74%{
			opacity: 0;
			filter: alpha(opacity=0);
		}
		75%{
			opacity: 0;
			filter: alpha(opacity=0);
			top: 50px;
			
		}
		100%{
			top: 10px;
		}
	}
	@keyframes upward_hover{
		0%{
			opacity: 1;
			filter: alpha(opacity=100);
		}
		73%{
			top: -50px;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		74%{
			opacity: 0;
			filter: alpha(opacity=0);
		}
		75%{
			opacity: 0;
			filter: alpha(opacity=0);
			top: 50px;
			
		}
		100%{
			top: 10px;
		}
	}
.lined-t-b{
	padding-top: 20px;
	padding-bottom: 20px;
	border-top: 1px solid #C0C0C0 ;
	border-bottom: 1px solid #C0C0C0;
}
.lined-b{
	border-bottom: 1px solid #C0C0C0;
}
.line-15
{
	height: 15px;
}
.black-color *
{
	color: black !important;
}
	.black-color .upward
	{
		border-color: black;
	}

.turquoise{
	background-color:#89CCCA;

}
.light-turquoise
{
	-webkit-transition-duration: 0.5s;
	        transition-duration: 0.5s;
	border-top: 10px solid #DCF0EF;
	background-color: #DCF0EF;
	color: #8ACCCA;
	text-align: center;
}
	.light-turquoise:hover{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		background-color: white;
		border-top: 10px solid rgba(137, 204, 202, 1);
		color: black;
	}
.white{
	color: black;
	background-color: white;
}
	.white *{
		color: black;
	}
.light-gray{

	background-color: #E4E4E4;
}
	.light-gray *{
		color: black;
	}
.medium-gray{
	background-color: #6C7377;
}
	.medium-gray p
	{
		color: #858B8F;
	}
.yellow{
	background-color: #FFCD02;
}	.yellow *{
		color: black;
	}

.dark-gray{
		background-color: #2B2B2B;
	}
	.dark-gray p
	{
		color: gray;
	}
.gray-blue{
	background-color: #242024;
}
	.gray-blue p{
		color: gray;
	}
.beige{
	background-color: #B9B5AE;
}
 	.beige p{
 		color: #D9D7D3;
 	}

.line-color-1
{
	background: #CACDCC;
}
.line-color-2{
	background: #929695;
}
.line-color-3{
	background: #C4DEDD;
}

.drop-down{
	overflow: visible;
	z-index: 10;
	padding-top: 20px !important;
	padding-left: 20px !important;
	padding-right: 20px !important;
}
.closest-salon
{
	background-color: #E4E4E4;
	overflow: visible;
	z-index: 10;
}
	.closest-salon h2, .closest-salon a, .closest-salon p{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		color: black;
	}
	.selector{
		position: relative;
		z-index: 2;
		background-color: white;
		border-left: 5px solid white;
	}
	.selector, .child-sel a {
		display: inline-block;
		padding: 15px !important;
		width: 100%;
		height: 50px !important;
	}
		.selector.child-sel{
			background-color: #EEE;
			border-left: 5px solid #EEE;

		}
		.child-sel.ch{
			background-color: #DDD;
			border-left: 5px solid #DDD;
		}
	.parent-sel{
		display: block;
	}
		.selector:hover, .selector.child-sel:hover,  .child-sel.ch:hover
		{
			-webkit-transition-duration: 0.5s;
			        transition-duration: 0.5s;
			color: #888B90;
			border-left: 5px solid #89CCCA;
		}


	ul.parent-sel, ul.child-sel{
		overflow: hidden;
		margin-left: 0 !important;
		max-height: 0;
		-webkit-transition: max-height 0.5s ease-in-out;
		        transition: max-height 0.5s ease-in-out;
	}

	.parent-sel.opened, .child-sel.opened{
		max-height: 1000px;
		-webkit-transition: max-height 0.5s ease-in-out;
		        transition: max-height 0.5s ease-in-out;	
	}

.offers *{
	color: black ;
}
	.offers .products{
			border-left: 1px solid #C0C0C0;
	}
	.categories{
		margin-left: 0;
	}
	.categories li {
		padding: 10px !important;

		-webkit-transition-duration: 0.5s;

		        transition-duration: 0.5s;
		border-left: 5px solid white;
	}
		.categories li a{
			display: inline-block;
		}
		.categories li:hover{
			border-left: 5px solid #89CCCA;
			background-color: #DDD;
		}
			.categories li:hover a
			{
				color: gray;
			}

.line-l-r{
	border-left: 1px solid #C0C0C0;
	border-right: 1px solid #C0C0C0;
}
	.contact-form input,
	.contact-form input:focus,
	.contact-form textarea{
		width: 90%;
		border: none;
		border-bottom: 1px solid #C0C0C0;
		box-shadow: none;

	}
	.contact-form input[type="checkbox"]{
		width: auto;
		margin-right: 5px; 
	}
	.contact-form textarea{
		width: 100%;
		resize: vertical;
	}
	.contact-form input[type="submit"]
	{
		width: 200px;
		border: none;
		padding: 20px;
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		color: white !important;
		background: #89CCCA;
		cursor: pointer;
	}
	.contact-form input[type="submit"]:hover{
		-webkit-transition-duration: 0.5s;
		        transition-duration: 0.5s;
		color: black !important;
	}
.interesting p{
	color: gray;
	position: absolute;
	top: 50%;
	left: 50%;
	text-align: center;
	display: block;
	-webkit-transform: translate(-50%,-50%);
	    -ms-transform: translate(-50%,-50%);
	        transform: translate(-50%,-50%);
	width: 90%;
	font-size: 24px;
}
	.interesting p span{
		font-size: 48px;
		color: gray;
	}
/*Footer*/
footer{
	background-color: white;
	
}
footer .footer *{
	color: gray;
	font-size: 12px;
}
h4{
	color: black !important;
	font-size: 16px !important;
	padding-bottom: 5px;
	border-bottom: 1px solid #C0C0C0;
	margin-bottom: 5px;
	width: 90%;
}
	footer ul{
		list-style: inside;
		list-style-type: circle;

	}
	footer ul li{
		margin-top: 10px;
	}
	footer ul li:hover a 
	{
		color: black;
		text-decoration: underline;

	}
.footer div{
	color: #383238!important;
}
.soc1{
	background-color: #3B5998;
}
.soc2{
	background-color: #CC2127;
}
/* Kiegészítés */
@media only screen and (max-width: 77.5em)/*<1240*/{
	.hide-for-xlarge-down {
		display: none !important;
	}
	h2{
		font-size: 15px;
	}

}
@media only screen and (max-width: 64em)/*<1024*/{
	.logo a {
		background-size: 525px 210px;
		width: 30px !important;
		height: 40px !important;
	}

}
@media only screen and (max-width: 45em)/*<720px */{
	h2{
		font-size: 18px;
	}

	.fixable
	{
		position: relative !important;
		width: 100% !important;
	}
	.fixable.topped{
		top: 0px;
	}
	div.title, div.title.lower{
		border-top: 50px;
	}

	.hide-for-720-down{
		display: none !important;
	}
	.show-for-720-down{
		display: inline-block !important;
	}
	.size-720-2{
		width: 16.666%;
	}
	.size-720-3{
		width: 25%;
	}
	.size-720-4
	{
		width: 33.3333%;
	}
	.size-720-6{
		width: 50%;
	}
	.size-720-12{
		width: 100%;
	}
	.size-720-pull-4
	{
		margin-left: 33.3333%!important;
	}
	.push-3{
		left: 0% !important;
	}
	header{
		top: 0px !important;
		height: 50px;
		background-color: #444;

		position: relative;
		top: 0;
		left: 0;
	}
	header .logo, header .ul{
		height: 40px !important;
	}
		nav {
			width: 100%!important;
			float: none;
			height: 50px;
		}
		nav .nav a.top-bord{
			padding: 0 !important
		}
		.top-bord{

			background-color: #242024;
			height: 50px !important;
			padding: 0 !important;
			line-height: 10px !important;			

		}
		.mobile-nav a 
		{
			background-color: #6C7377!important;			
		}
			.mobile-nav a .line-1,.mobile-nav a .line-2,.mobile-nav a .line-3{
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				position: relative;
				display: block;
				margin-top: 9px;
				margin-bottom: -3px;
				margin-left: auto;
				margin-right: auto;
				width: 30px !important;
				height: 4px !important;
				background-color: white;
			}
			.mobile-nav a .opened .line-1{
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				-webkit-transform: rotate(-45deg) translate(-7px, 7px);
				    -ms-transform: rotate(-45deg) translate(-7px, 7px);
				        transform: rotate(-45deg) translate(-7px, 7px);
			}
			.mobile-nav a .opened .line-2{
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				-webkit-transform: rotateY(90deg);
				        transform: rotateY(90deg);
			}
			.mobile-nav a .opened .line-3{
				-webkit-transition-duration: 0.5s;
				        transition-duration: 0.5s;
				-webkit-transform: rotate(45deg) translate(-7px, -7px);
				    -ms-transform: rotate(45deg) translate(-7px, -7px);
				        transform: rotate(45deg) translate(-7px, -7px);
			}
				.mobile{
					position: absolute;
					background-color:#6C7377;
					z-index: 2;
					overflow: hidden;
					color:white;

					font-size: 12px;
					
					max-height: 0 !important;
					-webkit-transition: max-height 0.5s ease-out;
					        transition: max-height 0.5s ease-out;
					
				}
					.mobile.opened{
						max-height: 1000px !important; 
						-webkit-transition: max-height 0.5s; 
						        transition: max-height 0.5s;

						
						padding-left: 0;
					}
					.mobile .lan, .mobile .copyright{
						margin-left: 20px;
						padding-top: 10px;
						padding-bottom: 10px;
						font-size: 10px;
					}
					.mobile .face
					{
						height: auto !important;
						margin-left: 5px;
						border-top: 1px solid #7E8488; 
					}
						.face .fb{
							float:none;

						}

					.mini-nav{
						height: auto !important;
					}
					.mini-nav ul{
						margin: 0;
						height: auto !important;
					}
					.mini-nav ul li{
						padding-top: 10px;
						padding-bottom: 10px;
						border-top: 1px solid #7E8488;
						cursor: pointer;
					}
					.mini-nav ul li:hover{
						background-color: #7E8488;
					}
					.mini-nav ul li a{
						height: 20px !important;
						padding: 0 !important;
						margin-left: 20px;
						font-size: 10px;
					}


	.cnt{
		right: 0;
		width: 16.666%;
		padding-bottom: 16.666%;
	}
		.cnt span{
			font-size: 48px; 
		}


	#trig-furn-menu{
		width: 50% !important;
	}

	.tile{
		padding-bottom: 50% !important;
	}
		.tile.pad-720
		{
			height: 0 !important;
			padding-bottom: 50%!important;
		}
		.closest-salon
		{
			height: auto !important;
			padding-bottom: 0 !important;
		}
	.double-tile{
		height: auto !important;
		padding-bottom: 0 !important;
	}
	.tile-120{
		height: 60px !important;
		padding-bottom: 100px !important;
	}

}
@media only screen and (max-width: 40.63em){
	.active{
		display: none;
	}
}
@media only screen and (max-width: 28.125em)/*<450px*/{
	h2{
		font-size: 28px;
	}
	.large-title{
		font-size: 1.3em!important;
	}
	.newsslider, .collslider{
		padding-top: 30px !important;
	}
	.newsslider .slick-next, .newsslider .slick-prev,
	.collslider .slick-next, .collslider .slick-prev
	{
		background-color: #242024;
		display: inline !important;
		top: 0;
		height: 30px;
		width: 50%;
	}
	.w200{
		max-width: 200%;
		height: auto;
		width: 200%;
	}
		.slider img{
			max-width: 200%;
			height: auto;
			width: 200%;
		}

	.cnt span{
		font-size: 28px;
	}
	.mini-100
	{
		width: 100% !important;
	}
	.mini-50{
		width: 50% !important;
	}
	.tile
	{
		padding-bottom: 100% !important;
	}
	.tile.pad-720
	{
		height: auto !important;
		padding-bottom: 0 !important;
	}
	.half-tile{
		padding-bottom: 50% !important;
	}
		.half-tile.tile-in-50{
			padding-bottom: 100%!important;
		}
		.news-offer{
			padding-bottom: 50% !important;
		}
		.drop-down, .closest-salon
		{
			height: auto !important;
			padding-bottom: 0 !important;
		}
	.tile-120{
		height: 0 !important;
		padding-bottom: 25% !important;
	}

}

@media only screen and (min-width: 45.063em)/*>720px*/{
	.cnt span{
		font-size: 25px;
	}
}

@media only screen and (min-width: 90.063em)/*>1440px*/{
	.cnt span{
		font-size: 45px;
	}
}


/*****DEBUG******/
.cake-error,
.cake-debug-output,
.cake-debug{
	margin-top: 180px;
	background: #ffcc00;
	background-repeat: repeat-x;
	background-image: -moz-linear-gradient(top, #ffcc00, #E6B800);
	background-image: -ms-linear-gradient(top, #ffcc00, #E6B800);
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ffcc00), to(#E6B800));
	background-image: -webkit-linear-gradient(top, #ffcc00, #E6B800);
	background-image: -o-linear-gradient(top, #ffcc00, #E6B800);
	background-image: linear-gradient(top, #ffcc00, #E6B800);
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	border: 1px solid rgba(0, 0, 0, 0.2);
	margin-bottom: 18px;
	padding: 7px 14px;
	color: #404040;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.25);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.25);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.25);
}


/***SCAFFOLD***/
.scaffold{
	margin-top: 180px;
}