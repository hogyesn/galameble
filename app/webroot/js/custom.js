$(document).ready(function(){     

     if(window.pageYOffset == 0 && $("header").attr("class").split(' ')[0]=="index"){
          $("header").removeClass("scrolled");
     }

     var scrolly = 0;
     var scrollDif = 0;
     var furnNavOpened = false;
     $(window).on("scroll",function(){
          scrollDif = window.pageYOffset - scrolly;
          console.log(window.pageYOffset);
          if(window.pageYOffset == 0 && $("header").attr("class").split(' ')[0]=="index"){
               $("header").removeClass("scrolled");
          }
          if (window.pageYOffset>=1 && window.pageYOffset<85){
               $("header").addClass("scrolled").removeClass("hidden");
          }
          if (window.pageYOffset>=85){
               $(".fixable").removeClass("topped");
               $("header").addClass("hidden");
               if(scrollDif<0){
                    $(".fixable").addClass("topped");
                    $("header").removeClass("hidden");
               }
          }
          if(window.pageYOffset >=200){
               $(".fixable").addClass("fixed");
          }
          if(window.pageYOffset < 200){
               $(".fixable").removeClass("fixed");
          }
          if(furnNavOpened){
               $("header").removeClass("hidden");
          }
          scrolly = window.pageYOffset;
     });

     //slider script
     $('.slider').slick({
          autoplay: true,
          fade: true,
          autoplaySpeed: 5000,
          appendDots: $(".bands"),
          dots: true,
          arrows: true,
          appendArrows: $(".arrows")

     });



     //navigation script
     var mobMenuOpened = false;
     $(".mobile-nav > a > .nav").click(function(e){
          e.preventDefault();
          if (mobMenuOpened){
               $(this).removeClass("opened");
               $(".mobile").removeClass("opened");
               mobMenuOpened = false;
          }else{
               $(this).addClass("opened");
               $(".mobile").addClass("opened");
               mobMenuOpened = true;
          }
     });

     $("#trig-furn-menu").click(function(e){
          e.preventDefault();
          if(furnNavOpened){
               $(".furn-menu").removeClass("opened");
               furnNavOpened = false;
               $("main").removeClass("opacity");
          }else{
               $(".furn-menu").addClass("opened");
               furnNavOpened = true;
               $("main").addClass("opacity");
          }
     });

     $(".x").click(function(e){
          $(".furn-menu").removeClass("opened");
          furnNavOpened = false;
          $("main").removeClass("opacity");
     });

     $(".trig.furn ").click(function(e){
          e.preventDefault();
          $(".furn-bar").removeClass("inactive");
          $(".trig.active").removeClass('active');
          $(this).addClass("active");
     });
     $(".trig.nowad ").click(function(e){
          e.preventDefault();
          $(".furn-bar").addClass("inactive");
          $(".trig.active").removeClass('active');
          $(this).addClass("active");
     });

     $('.newsslider').slick({
          slidesToShow: 3,
          responsive: [{
               breakpoint: 720,
               settings:{
                    slidesToShow:1
               }
          }]
     });

     $('.collslider').slick({
          slidesToShow: 4,
          responsive: [{
               breakpoint: 720,
               settings:{
                    slidesToShow:1
               }
          }]
     });

     var selects = new Array();
     $(".selector").click(function(e){
          var item = "."+$(this).attr("class").split(" ")[1]+"."+$(this).attr("class").split(" ")[2]+".ch";
          $(this).removeClass("nonselected");
          $(this).addClass("selected");
          if (!selects[item] || selects[item]==false){
               selects[item]=true;
               $(item).addClass("opened");
          }else{
               selects[item]=false;
               $(item).removeClass("opened");
          }
     }); 




});

$('a[href^="#"]').on('click',function (e) {
     e.preventDefault();

     var target = this.hash;
     $target = $(target);

     $('html, body').stop().animate(
          {'scrollTop': $target.offset().top},
          900,
          'swing',
          function () {
               window.location.hash = target;
          }
     );
});

$(".func").hover(function(){
     var func = $(this).attr("class").split(" ")[2];
     var content = $("."+func+" > .desc").html();
     $(".changable").html(content);
});

$(".func").mouseleave(function(){
     $(".changable").html("Funkciók:");
});

$(function(){
     $( "#contact-accordion" ).accordion({
          collapsible: true,
          heightStyle: "content"
     });    
});

$(function(){

});