<div class="row title turquoise">
    <h1>Családok</h1>
</div>
<div class="row offers">
    <div class="small-12 size-720-12 medium-3 large-3 column nav">
        <ul class="categories fixable">
            <?php foreach ($families as $family): ?>
                <li>
                    <a href=<?php echo '#'.$family['hash']; ?>><?php echo $family['name']; ?></a>
                </li>
            <?php endforeach; ?>
            <hr/>
            <li><a href=<?php echo $this->webroot.'butor/'; ?>>Típus szerint</a></li>
        </ul>
    </div>
    <div class="small-12 size-720-12 medium-9 large-9 column products">
        <!-- Kategória Cím nagy képpel -->

        <?php foreach ($families as $family): ?>
            <div id=<?php echo $family['hash']; ?> class="small-12 size-720-12 medium-12 large-12 column large-img">
                <img src=<?php echo $family['image']['src']; ?> alt=<?php echo $family['image']['alt'] ?>>
                <span class="large-title"><?php echo $family['name'] ?></span>
            </div>
            <!-- Ezután jönnek ilyen formában a bútorok -->
            <?php foreach ($family['furnitures'] as $key => $furniture): ?>
                <div class="mini-100 small-12 size-720-6 medium-4 large-4 column">
                    <a class="nowadays" href=<?php echo $furniture['sheetUrl']; ?>><img src=<?php echo $furniture['image']['src']; ?> alt=<?php echo $furniture['image']['alt']; ?>>
                      <span class="hover-title">Többféle változatban</span>
                        <span class="title"><?php echo $furniture['name'] ?></span>
                    </a>
                </div>
            <?php endforeach; ?>

        <?php endforeach; ?>

    </div>
</div>