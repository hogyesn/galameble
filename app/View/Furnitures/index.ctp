<div class="row title turquoise">
    <h1>Ajánlataink</h1>
</div>
<div class="row offers">
    <div class="small-12 size-720-12 medium-3 large-3 column nav">
        <ul class="categories fixable">
            <?php foreach ($furnitureTypes as $furnitureType): ?>
            <li><a href=<?php echo '#'.$furnitureType['hash']; ?>><?php echo $furnitureType['name']; ?></a></li>
            <?php endforeach; ?>
            <hr/>
            <li><a href=<?php echo $this->webroot.'butor_csalad/'; ?>>Családok szerint</a></li>
        </ul>
    </div>
    <div class="small-12 size-720-12 medium-9 large-9 column products">
        <!-- Kategória Cím nagy képpel -->

        <?php foreach ($furnitureTypes as $furnitureType): ?>
            <div id=<?php echo $furnitureType['hash']; ?> class="small-12 size-720-12 medium-12 large-12 column large-img">
                <img src=<?php echo $furnitureType['image']['src'] ?> alt=<?php echo $furnitureType['image']['alt']; ?>>
                <span class="large-title"><?php echo $furnitureType['name'] ?></span>
            </div>

            <!-- Ezután jönnek ilyen formában a bútorok -->
            <?php foreach ($furnitureType['furnitures'] as $key => $furniture): ?>
                <div class="mini-100 small-12 size-720-6 medium-4 large-4 column">
                    <a class="nowadays" href=<?php echo $furniture['sheetUrl']; ?>>
                        <img src=<?php echo $furniture['image']['src']; ?> alt=<?php echo $furniture['image']['alt']; ?>>
                        <span class="hover-title">Többféle változatban</span>
                        <span class="title"><?php echo $furniture['name'] ?></span>
                    </a>
                </div>
            <?php endforeach; ?>

        <?php endforeach; ?>

    </div>
</div>
