<div class="row title white">
    <h1><?php echo $furniture['name']; ?></h1>
</div>
<div class="row light-gray">
    <div class="mini-100 small-12 size-720-12 medium-6 large-6 column">

        <!-- Galéria nagy kép -->
        <div class="mini-100 small-12 medium-12 large-12 column">
            <a href=<?php echo $furniture['mainImage']['src']; ?> data-lightbox="img">
                <img src=<?php echo $furniture['mainImage']['src']; ?> alt=<?php echo $furniture['mainImage']['alt']; ?>>
            </a>
        </div>

        <!-- Galéria kis képek -->

        <?php foreach ($furniture['images'] as $key => $image): ?>
            <div class="small-4 medium-4 large-4 column">
                <a class="opacitive white" href=<?php echo $image['src']; ?> data-lightbox="img">
                    <img src=<?php echo $image['src']; ?>>
                </a>
            </div>
        <?php endforeach; ?>
    </div>


    <!-- Hash name css osztályhoz pl func-tv 
    DB:: ha null a kényszer, akkor nem mutat, egyébként igen
    -->
    <div class="mini-100 small-12 size-720-12 medium-6 large-6 column padding-20">
        <div class="row padding-20">
            <h3 class="changable">Funkciók:</h3>
            <div class="column ">
                <span class="icon func func1"><span class="desc">leírás1</span></span>
                <span class="icon func func2"><span class="desc">leírás2</span></span>
                <span class="icon func func3"><span class="desc">leírás3</span></span>
                <span class="icon func func4"><span class="desc">leírás4</span></span>
                <span class="icon func func5"><span class="desc">leírás5</span></span>
                <span class="icon func func6"><span class="desc">leírás6</span></span>
                <span class="icon func func7"><span class="desc">leírás7</span></span>
                <span class="icon func func8"><span class="desc">leírás8</span></span>
                <span class="icon func func9"><span class="desc">leírás9</span></span>
                <span class="icon func func10"><span class="desc">leírás10</span></span>
                <span class="icon func func11"><span class="desc">leírás11</span></span>
                <span class="icon func func12"><span class="desc">leírás12</span></span>
                <span class="icon func func13"><span class="desc">leírás13</span></span>
                <span class="icon func func14"><span class="desc">leírás14</span></span>
                <span class="icon func func15"><span class="desc">leírás15</span></span>
                <span class="icon func func16"><span class="desc">leírás16</span></span>
                <span class="icon func func17"><span class="desc">leírás17</span></span>
                <span class="icon func func18"><span class="desc">leírás18</span></span>
            </div>
        </div>


        <div class="row padding-20">
            <h3>Méretek</h3>
            <div class="small-12 medium-6 large-6 column">
                <img src="http://www.galameble.com/wp-content/uploads/naroznikbelluno.jpg">
            </div>
            <div class="small-12 medium-6 large-6 column padding-20">
                <p>méretleírás</p>
            </div>
        </div>
        <div class="row padding-20">
            <h3>Ár</h3>
            <div class="small-12 medium-6 large-6 column padding-20">                       
                <p>árajánlat</p>
            </div>
        </div>

    </div>
</div>
