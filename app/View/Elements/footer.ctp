<div class="row">
    <div class="size-720-12 medium-6 large-6 column tile">
        <h2>Gala Collezione - Piato</h2>
        <p>Célunk, hogy változatos és szép bútorokat hozzunk létre. A gyártás során csak vágás és a varrás történik géppel, a többi munkát manuálisan végzik. A rugalmasság biztosított az ügyfeleknek a vásárlás során</p>
        <img class="abs w200" src="http://www.galameble.com/wp-content/uploads/gm-utils-02.jpg"/>
    </div>
    <div class="size-720-6 mini-100  small-12 medium-3 large-3 column tile ht-parent">
        <div class="soc1 inc-line large-12 column half-tile">
            <h2>Kövessen Facebookon</h2>
        </div>
        <div class="soc2 inc-line large-12 column half-tile">
            <h2>Kövesse a híreket Pinteresten</h2>
        </div>
    </div>
    <div class="opacitive inc-line mini-100 small-12 size-720-6 medium-3 large-3 column tile">
        <h2>Online katalógus 2014</h2>
        <img class="abs" src="http://www.galameble.com/wp-content/uploads/katalo_bg.jpg"/>
    </div>
</div>
<div class="row footer padding-20">
    <div class="mini-100 small-12 size-720-12 medium-6 large-6 column">
        <div class="mini-100 small-12 size-720-6 medium-6 large-6 column">
            <h4>Minden bútor</h4>
            <ul>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
            </ul>
        </div>
        <div class="mini-100 small-12 size-720-6 medium-6 large-6 column">
            <h4>Jó tudni</h4>
            <ul>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
            </ul>
        </div>
        <div class="mini-100 small-12 size-720-6 medium-6 large-6 column">
            <h4>Kapcsolat</h4>
            <ul>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
            </ul>
        </div>
    </div>
    <div class="mini-100 small-12 size-720-12 medium-6 large-6 column">
        <div class="mini-100 small-12 size-720-6 medium-6 large-6 column">
            <h4>Egyéb</h4>
            <ul>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
                <li><a>Menüpont</a></li>
            </ul>
        </div>
        <div class="mini-100 small-12 size-720-6 medium-6 large-6 column">
        <h4>Kapcsolat</h4>
        <ul>
            <li><a>Menüpont</a></li>
            <li><a>Menüpont</a></li>
            <li><a>Menüpont</a></li>
        </ul>
        </div>
    </div>
    <div class="lined-b column"></div>
</div>