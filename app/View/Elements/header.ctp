<?php 
    $headerClass = $action=="pages/display"?"index scrolled":"scrolled";
?>
<header class=<?php echo '"'.$headerClass.'"'; ?>>
    <div class="row furn-menu">
        <div class="large-12 furn-nav">
            <div class="small-4 medium-4 large-2 trig furn column active"><a>Bútor</a></div>
            <div class="small-4 medium-4 large-2 trig nowad column"><a>Mostanában</a></div>
            <div class="small-8 medium-4 large-4  column">
                <div class="small-6 medium-6 large-6 search column"><a><span class="icon src"></span></a></div>
                <div class="small-6 medium-6 large-6 x column"><a><span class="icon x"></span></a></div>
            </div>
        </div>
        
        <div class="double">
            <div class="small-12 medium-12 large-12 furn-bar">
                <div class="small-6 medium-6 large-6 column">
                    <span class="black-bg">
                        <?php foreach ($furnitureTypesForHeader as $furnitureType): ?>
                        <a class="small-6 medium-4 large-2 column opacitive" href="<?php echo $this->webroot; ?>ajanlat/#<?php echo $furnitureType['FurnitureType']['hash'] ?>">
                            <img class="furnitures" src="<?php echo $this->webroot.'img/images/furniture_types/'.$furnitureType['Image_idx']['name']; ?>"/>
                            <span class="border"></span>
                            <span class="title"><?php echo $furnitureType['FurnitureType']['name'] ?></span>
                        </a>
                        <?php endforeach; ?>
                    </span>
                </div>
                <div class="small-6 medium-6 large-6 column">
                    <!--  Ide majd valami újdonságok jönnek random  -->
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                    <a class="small-6 medium-4 large-2 column nowadays" href="#">
                        <img src="http://www.galameble.com/wp-content/uploads/barista_kuchnia_miniatura.jpg"/>
                        <span class="title">Bútor neve</span>
                        <span class="hover-title">Többféle változatban</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <span class="logo medium-2 large-2 size-720-4"><a href="http://www.galameble.hu/"></a></span>
        <nav class="medium-10 large-10">
            <ul class="nav">
                <li class="small-2 medium-2 large-2 size-720-6" id="trig-furn-menu">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="icon furn hide-for-720-down"></span>
                            <span class="text hide-for-xlarge-down show-for-720-down">Bútor</span>
                        </span>
                    </a>
                </li><li class="small-2 medium-2 large-2 hide-for-720-down">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="icon places"></span>
                            <span class="text hide-for-xlarge-down">Üzletek</span>
                        </span>
                    </a>
                </li><li class="small-2 medium-2 large-2 hide-for-720-down">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="icon wkning"></span>
                            <span class="text hide-for-xlarge-down">Jó tudni</span>
                        </span>
                    </a>
                </li><li class="small-2 medium-2 large-2 hide-for-720-down">
                    <a class="top-bord"><span class="nav">
                            <span class="icon contact"></span>
                            <span class="text hide-for-xlarge-down">Kapcsolat</span>
                        </span>
                    </a>
                </li><li class="small-1 medium-1 large-1 hide-for-720-down">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="icon fb"></span>
                        </span>
                    </a>
                </li><li class="small-1 medium-1 large-1 hide-for-720-down">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="text">HU</span>
                        </span>
                    </a>
                </li><li id="x-lines" class="size-720-2 mobile-nav show-for-720-down">
                    <a class="top-bord">
                        <span class="nav">
                            <span class="line-1"></span>
                            <span class="line-2"></span>
                            <span class="line-3"></span>
                        </span>
                    </a>
                </li>
            </ul>
            <div class="mobile show-for-720-down">
                <div class="lan"><span><a>HU</a><a>PL</a></span></div>
                <div class="mini-nav">
                    <ul>
                        <li class="places"><span><a>Üzletek</a></span></li>
                        <li class="wkning"><span><a>Jó tudni</a></span></li>
                        <li class="contact"><span><a>Kapcsolat</a></span></li>
                    </ul>
                </div>
                <div class="face"><span class="icon fb"></span></div>
                <div class="copyright">Szöveg</div>
            </div>
        </nav>
    </div>
</header>