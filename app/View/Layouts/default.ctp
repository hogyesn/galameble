<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */


?>
<!DOCTYPE html>
<html>
<head>
	<?php //echo $this->Html->charset(); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1">
	<title>
		GALAMEBLE
	</title>
	<?php
		echo $this->Html->meta('icon');


		echo $this->Html->script('jquery.js');
		
		echo $this->Html->script('eqheight.js');
		
		echo $this->Html->script('lightbox.js');

		echo $this->Html->script('slick.js');

		

		echo $this->fetch('meta');
		echo $this->fetch('script');
	?>
	<link href="/caketest/cakephp-cakephp-162c39a/favicon.ico" type="image/x-icon" rel="icon" />
	<link href="/caketest/cakephp-cakephp-162c39a/favicon.ico" type="image/x-icon" rel="shortcut icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/foundation.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/slick.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/lightbox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/custom.php?webroot=<?php echo $this->webroot; ?>" />

	<script>
		 
	$(window).load(function(){
		$(".gm-preloader-img").fadeOut();
		$(".gm-preloader").delay(1000).fadeOut("fast");
	});

	</script>
</head>
<body>
	<div class="gm-preloader"><div class="gm-preloader-img"></div></div>
	<?php echo $this->element('header'); ?>
	<div id="container">
		<main>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</main>
		<footer>
			<?php echo $this->element('footer'); ?>
			<?php /*echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false)
				);*/
			?>
		</footer>
	</div>
	<?php 
		echo $this->Html->script('custom.js');
		echo $this->element('sql_dump');
	?>
</body>
</html>
