
<div class="row">
    <div class="large-12 column slider-container">
        <div class="slider">
            <?php foreach ($sliderImages as $key => $image): ?>
            <div>
                <div class = "cnt">
                    <span>0<?php echo $key+1; ?></span>
                </div>
                <img src="<?php echo $this->webroot.'img/images/header_slider/'.$image['Image']['name']; ?>"/>
                <span class="gradient"></span>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="slider-nav small-12 medium-12 large-12 column">
            <div class="bands small-10 medium-10 large-10"><!-- nav --></div>

        </div>
        <div class="arrows hide-for-720-down"><!-- but --></div> 
    </div>
</div>

<div class="row">    
        
    <span class="black-bg">
        <div class="large-12">                                        
            <div class="medium-12">
                <?php for($i=0; $i<2; $i++): ?>                                     
                <div class="large-3 medium-3 size-720-6 small-6 panel column">       
                    <a class="opacitive" href="<?php echo $this->webroot.'butor/#'.$furnitureTypes[$i]['FurnitureType']['hash'] ?>">
                        <img class="furnitures" src="<?php echo $this->webroot.'img/images/furniture_types/'.$furnitureTypes[$i]['Image_idx']['name']; ?>"/>
                        <span class="border"></span>
                        <span class="title"><?php echo $furnitureTypes[$i]['FurnitureType']['name']; ?></span>
                    </a>
                </div>
                <?php endfor; ?>
            </div>
            <div class="medium-12">
                <?php for($i=2; $i<4; $i++): ?>                                     
                <div class="large-3 medium-3 size-720-6 small-6 panel column">       
                    <a class="opacitive" href="<?php echo $this->webroot.'butor/#'.$furnitureTypes[$i]['FurnitureType']['hash'] ?>">
                        <img class="furnitures" src="<?php echo $this->webroot.'img/images/furniture_types/'.$furnitureTypes[$i]['Image_idx']['name']; ?>"/>
                        <span class="border"></span>
                        <span class="title"><?php echo $furnitureTypes[$i]['FurnitureType']['name']; ?></span>
                    </a>
                </div>
                <?php endfor; ?> 
            </div>
        </div>
    </span>
</div>

<div class="row news resizable large-12">
    <div class="mini-100 size-720-6 medium-3 large-3 column news-offer tile">
        <h2>Újdonságok</h2>
        <p>Nézze át új termékeink széles skáláját</p>
    </div>
    <div class="mini-100 size-720-6 medium-9 large-9 news-slider column">
        <div class="newsslider">
            <div>
                <a class="nowadays" href="#"><img src="http://www.galameble.com/wp-content/uploads/inari_fotele_miniatura.jpg"/>
                <span class="hover-title">Többféle változatban</span>
                <span class="title">Bútor neve</span>
                </a>
            </div>
            <div>
                <a class="nowadays" href="#"><img src="http://www.galameble.com/wp-content/uploads/inari_fotele_miniatura.jpg"/>
                <span class="hover-title">Többféle változatban</span>
                <span class="title">Bútor neve</span>
                </a>
            </div>
            <div>
                <a class="nowadays" href="#"><img src="http://www.galameble.com/wp-content/uploads/inari_fotele_miniatura.jpg"/>
                <span class="hover-title">Többféle változatban</span>
                <span class="title">Bútor neve</span>
                </a>
            </div>
            <div>
                <a class="nowadays" href="#"><img src="http://www.galameble.com/wp-content/uploads/inari_fotele_miniatura.jpg"/>
                <span class="hover-title">Többféle változatban</span>
                <span class="title">Bútor neve</span>
                </a>
            </div>
            <div>
                <a class="nowadays" href="#"><img src="http://www.galameble.com/wp-content/uploads/inari_fotele_miniatura.jpg"/>
                <span class="hover-title">Többféle változatban</span>
                <span class="title">Bútor neve</span>
                </a>
            </div>
            
        </div>

    </div>
</div>


<div class="row resizable">
    <div class="inc-line turquoise large-h-25 mini-100 size-720-6 medium-3 large-3 column tile">
        <h2>Tervezze meg saját moduljait 2D-ben!</h2>
    </div>
    <div class="inc-line opacitive mini-100 size-720-6 medium-3 large-3 column tile">    <img class="abs" src="http://www.galameble.com/wp-content/uploads/gm-utils-01.jpg"/>
        <h2>Ön dönti el, hogyan nézzen ki bútora</h2>
    </div>
    <div class="size-720-12 medium-6 large-6 column closest-salon  tile">
        <h2>Találja meg legközelebbi üzletünket</h2>
        <p>Keresse forgalmazóinkat, és fedezze fel a bútorok szépségeit</p>
        <a class="selector parent-sel 1">
            Válassza ki a szalont!
            <span class="icon arr-down"></span>
        </a>
        <ul class="parent-sel 1 ch">
            <li>
                <a class="selector child-sel 1">Budapest</a>
                <ul class="child-sel 1 ch">
                    <li><a>Szalon</a></li>
                </ul>
                <a class="selector child-sel 2">Egyéb</a>
                <ul class="child-sel 2 ch">
                    <li><a>más</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>